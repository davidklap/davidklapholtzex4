#pragma once
#include <stdio.h>

class OutStream
{
public:
	OutStream();
	~OutStream();
	
	OutStream& operator<<(char *str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)());
	FILE* _file;
};



void endline();