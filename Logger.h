#include "FileStream.h"
#include "OutStream.h"
#pragma once

class Logger
{
public:
	Logger(const char *filename, bool logToScreen);
	~Logger();

	void print(const char *msg);

protected:
	bool _logToScreen; // the flag that says if the we need to olso too print to the string 
	OutStream _out;
	FileStream _file_strem;
	FILE* _file;
};
