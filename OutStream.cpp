#include "OutStream.h"
#include <stdio.h>


OutStream::OutStream()
{
	_file = stdout;
}

OutStream::~OutStream()
{
}

OutStream& OutStream::operator<<( char *str)
{
	fprintf(_file,"%s",str);
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(_file,"%d",num);
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)())
{
	pf();
	return *this;
}


void endline()
{
	printf("\n");
}
