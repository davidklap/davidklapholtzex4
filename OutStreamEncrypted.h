#include "OutStream.h"
#include  <cstdio>
#include <iostream>
#pragma once
#define MAX_NUM_OF_ASCII 126// the mux nuber that ascii can be 
#define MIN_NUM_OF_ASCII 32 // tje min niber that i asked to use 
class OutStreamEncrypted : public OutStream
{
public:
	OutStreamEncrypted(int keisar_code, const char* filename);
	~OutStreamEncrypted();
	OutStreamEncrypted& operator<<(std::string str);
	OutStreamEncrypted& operator<<(int num);
	OutStreamEncrypted& operator<<(void(*pf)());
	std::string change_str_with_code(std::string str);
private:
	int _keisar_code; // the failed of the code that wiil make the fILE secres
	FILE* _file;
};



