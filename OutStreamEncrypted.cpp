#include "OutStreamEncrypted.h"
#include "FileStream.h"

#include <string>
OutStreamEncrypted::OutStreamEncrypted(int keisar_code, const char* filename) :
	OutStream()
{
	_keisar_code = keisar_code;
	_file = crete_file(filename);
}

/*fonction get the string / chsr* to convert an print it tinto the fil when it after ceysar */

OutStreamEncrypted& OutStreamEncrypted ::operator<< (std::string str)
{
	str = change_str_with_code(str);
	std::cout << str;
	return *this;
}
 // fonction get thr str and vhange it eith ghe keisar cide 
std::string OutStreamEncrypted ::change_str_with_code(std::string str)
{
	int i = 0;
	for (; i<str.length(); i++)
	{
		if (int(str[i]) + _keisar_code > MAX_NUM_OF_ASCII) // if wuth the keisar is bigger than thhe max ascii change it 
		{
			str[i] = MAX_NUM_OF_ASCII - (str[i] + _keisar_code) + MIN_NUM_OF_ASCII; // GOING TO THE MIN PLACE THAT THE char sopse to b ein  case that its boger tha the kimit 
		}
		else
		{
			str[i] +=_keisar_code;
		}
	}
	return str;
}

OutStreamEncrypted& OutStreamEncrypted:: operator<<(int num)
{
	std::cout << num + _keisar_code;
	return *this;
}

OutStreamEncrypted::~OutStreamEncrypted()
{
}
